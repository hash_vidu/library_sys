/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author HANSAKA VIDURANGA
 */
public class db {
    
    private static Connection connection;
    
    private static void init_DB()throws Exception{
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/library_system", "root", "123");
        
    } 
    
    public static void push(String sql)throws Exception{
        if(connection==null){
            init_DB();
        }
        connection.createStatement().executeUpdate(sql);
        
    }
    
    public static ResultSet search(String sql)throws Exception{
        if(connection==null){
            init_DB();
        }
        return connection.createStatement().executeQuery(sql);
        
    }
    
}
